<?php
  $inifile = parse_ini_file("../config.ini");
  $conn = new mysqli("localhost", $inifile['username'], $inifile['password']);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $conn->select_db("lolchampiontags");

  $postdata = file_get_contents("php://input");
  $request = json_decode($postdata);

  if ($request->table == "champion") {
    if (property_exists($request, "tagIds")) {
      // SELECT name FROM products WHERE name IN ( 'Value1', 'Value2', ... );
      $tags = array();
      foreach($request->tagIds as $tagId => $value) {
        $tags[] = $value;
      }
      $tagsString = join(',', $tags);
      $numOfTags = count($tags);
      $result = $conn->query("SELECT champion.name, champion.image, champion.idChampion FROM champion
                              JOIN champion_has_tag AS cht ON champion.idChampion = cht.Champion_idChampion
                              JOIN tag ON cht.Tag_idTag = tag.idTag
                              WHERE tag.idTag IN ($tagsString)
                              GROUP BY champion.name
                              HAVING count(DISTINCT tag.name) = $numOfTags");
    } else {
      $result = $conn->query("SELECT * FROM champion");
    }
  } else if ($request->table == "tag") {
    $result = $conn->query("SELECT idTag, name, idCategoryOfTag FROM tag WHERE idCategoryOfTag=$request->idCategory");
  } else if ($request->table == "category") {
    $result = $conn->query("SELECT idCategory, name FROM category");
  } else if ($request->table == "champion_has_tag") {
    $result = $conn->query("SELECT * FROM champion_has_tag");
  }

  $dbData = array();
  while($r = mysqli_fetch_assoc($result)) {
    $dbData[] = $r;
  }
  $conn->close();

  echo json_encode($dbData);
?>