<?php
  $inifile = parse_ini_file("../config.ini");
  $conn = new mysqli("localhost", $inifile['username'], $inifile['password']);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $conn->select_db("lolchampiontags");

  $request = json_decode(file_get_contents("php://input"));

  $result = "";
  if ($request->function == "addTag") {
    foreach($request->champsIds as $iterator => $champId) {
      // add a row only if it didn't exist before
      $result = $result +
                $conn->query("INSERT INTO champion_has_tag (Champion_idChampion, Tag_idTag)
                                SELECT $champId, $request->tagId
                                  FROM dual
                                 WHERE NOT EXISTS (SELECT 1
                                                     FROM champion_has_tag
                                                    WHERE Champion_idChampion = $champId
                                                      AND Tag_idTag = $request->tagId);");
    }

  } else if ($request->function == "removeTag") {
    // DELETE FROM `champion_has_tag` WHERE `Champion_idChampion` = 12 AND `Tag_idTag` = 3
    $result = $conn->query("DELETE FROM champion_has_tag
                              WHERE Champion_idChampion = $request->champId AND
                                    Tag_idTag = $request->tagId;");
  }

  $conn->close();
  echo json_encode($result);
?>