﻿(function() {
  var app = angular.module('lolTags', []);

  app.controller('dbController', ['$scope', '$http', '$log', function($scope, $http, $log) {
    $scope.dbChampions = {};
    $scope.dbCategories = {};
    $scope.fieldsetStyle = {};
    $scope.categoriesReady = false;
    $scope.counter = 0;
    $scope.counterSelectedChampions = 0;

    $scope.$watch('categoriesTags', function(newValue, oldValue) {
      if ($scope.categoriesReady) {
        var selectedTags = [];
        for (var category in $scope.categoriesTags) {
          for (var tag in $scope.categoriesTags[category]) {
            if ($scope.categoriesTags[category][tag].selected) {
              selectedTags.push($scope.categoriesTags[category][tag].idTag);
            }
          }
        } // for category
        var args = { 'table' : "champion" };
        if (selectedTags.length) {
          args.tagIds = selectedTags;
        }
        // Create champions data (from dataBase and additional fields)
        $http.post('/www/getData.php', args).success(function(data, status) {
          $scope.dbChampions = {};
          data.forEach(function (champion) {
            $scope.dbChampions[champion.name] = champion;
            $scope.dbChampions[champion.name].isSelected = false;
          });
          $scope.counterSelectedChampions = 0;
        });
      }
    }, true);

    $http.post('/www/getData.php', { 'table' : "champion_has_tag" }).success(function(data, status) {
      $scope.championTagsMap = data;
    });

    $http.post('/www/getData.php', { 'table' : "category" }).success(function(categories, status) {
      $scope.dbCategories = categories;
      $scope.fieldsetStyle = { width: (100 / categories.length - 0.4428) + '%' };
      $scope.categoriesTags = {};  // needed to init object;

      $scope.categoriesTagsFetchedCount = 0;
      var categoriesCount = categories.length;

      categories.forEach(function(category) {
        $scope.categoriesTags[category.name] = {};

        // fetch tags for given category
        $http.post('/www/getData.php', { 'table' : "tag", 'idCategory' : category["idCategory"] }).success(function(tags, status) {
          tags.forEach(function(tag) {
            tag.selected = false;
            $scope.categoriesTags[category.name][tag.name] = tag;
          }); // foreach tag

          $scope.categoriesTagsFetchedCount++;
          if ($scope.categoriesTagsFetchedCount == categoriesCount) {
            $scope.categoriesReady = true;
          }
        }); // post
      }); // foreach category
    }); // post

    // function to select champions and count how many champions is selected
    $scope.selectChampion = function(championName) {
      if ($scope.dbChampions[championName].isSelected) {
        $scope.counterSelectedChampions--;
        $scope.dbChampions[championName].isSelected = false;
      }
      else {
        $scope.counterSelectedChampions++;
        $scope.dbChampions[championName].isSelected = true;
      }
      // /ng-click="champ[champ.name].isSelected = !champ[champ.name].isSelected">
    }

    $scope.addTagToSelected = function(tag) {
      var champsList = [];
      for (var champName in $scope.dbChampions) {
        if ($scope.dbChampions.hasOwnProperty(champName)) {
          if ($scope.dbChampions[champName].isSelected) {
            console.log($scope.dbChampions[champName].name);
            champsList.push($scope.dbChampions[champName].idChampion);
            // TODO: instead of individual requests, we should send one with all new rows combined
          }
        }
      } // foreach champ
      $http.post('/www/sendData.php',
        {
          'function' : "addTag",
          'champsIds' : champsList,
          'tagId' : tag.idTag
        }).success(function(data, status) {
          console.log(data);
          console.log(status);
        });
    }

    $scope.removeTagFromSelected = function(tag) {
      for (var champName in $scope.dbChampions) {
        if ($scope.dbChampions.hasOwnProperty(champName)) {
          if ($scope.dbChampions[champName].isSelected) {
            console.log($scope.dbChampions[champName].name);
            // TODO: instead of individual requests, we should send one with all new rows combined
            console.log("I want to delete: " + champName + " from tag " + tag.name);
            console.log($scope.dbChampions[champName]);
            $http.post('/www/sendData.php',
              {
                'function' : "removeTag",
                'champId' : $scope.dbChampions[champName].idChampion,
                'tagId' : tag.idTag
              }).success(function(data, status) {
                //
              });
            delete $scope.dbChampions[champName];
          }
        }
      } // foreach champ
    }

  }])
})();