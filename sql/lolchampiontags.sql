-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2015 at 06:02 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lolchampiontags`
--
DROP TABLE IF EXISTS `champion_has_tag`;
DROP TABLE IF EXISTS `champion`;
DROP TABLE IF EXISTS `tag`;
DROP TABLE IF EXISTS `category`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `idCategory` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idCategory`, `name`) VALUES
(1, 'Crowd Control'),
(2, 'Role'),
(3, 'Ownership'),
(4, 'Lane');

-- --------------------------------------------------------

--
-- Table structure for table `champion`
--

CREATE TABLE IF NOT EXISTS `champion` (
  `idChampion` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`idChampion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `champion`
--

INSERT INTO `champion` (`idChampion`, `name`, `image`) VALUES
(1, 'Annie', 'Annie_Square_0.png'),
(2, 'Olaf', 'Olaf_Square_0.png'),
(3, 'Galio', 'Galio_Square_0.png'),
(4, 'TwistedFate', 'TwistedFate_Square_0.png'),
(5, 'Xin Zhao', 'XinZhao_Square_0.png'),
(6, 'Urgot', 'Urgot_Square_0.png'),
(7, 'LeBlanc', 'Leblanc_Square_0.png'),
(8, 'Vladimir', 'Vladimir_Square_0.png'),
(9, 'Fiddlesticks', 'FiddleSticks_Square_0.png'),
(10, 'Kayle', 'Kayle_Square_0.png'),
(11, 'Master Yi', 'MasterYi_Square_0.png'),
(12, 'Alistar', 'Alistar_Square_0.png'),
(13, 'Ryze', 'Ryze_Square_0.png'),
(14, 'Sion', 'Sion_Square_0.png'),
(15, 'Sivir', 'Sivir_Square_0.png'),
(16, 'Soraka', 'Soraka_Square_0.png'),
(17, 'Teemo', 'Teemo_Square_0.png'),
(18, 'Tristana', 'Tristana_Square_0.png'),
(19, 'Warwick', 'Warwick_Square_0.png'),
(20, 'Nunu', 'Nunu_Square_0.png'),
(21, 'Miss Fortune', 'MissFortune_Square_0.png'),
(22, 'Ashe', 'Ashe_Square_0.png'),
(23, 'Tryndamere', 'Tryndamere_Square_0.png'),
(24, 'Jax', 'Jax_Square_0.png'),
(25, 'Morgana', 'Morgana_Square_0.png'),
(26, 'Zilean', 'Zilean_Square_0.png'),
(27, 'Singed', 'Singed_Square_0.png'),
(28, 'Evelynn', 'Evelynn_Square_0.png'),
(29, 'Twitch', 'Twitch_Square_0.png'),
(30, 'Karthus', 'Karthus_Square_0.png'),
(31, 'Cho''Gath', 'Chogath_Square_0.png'),
(32, 'Amumu', 'Amumu_Square_0.png'),
(33, 'Rammus', 'Rammus_Square_0.png'),
(34, 'Anivia', 'Anivia_Square_0.png'),
(35, 'Shaco', 'Shaco_Square_0.png'),
(36, 'DrMundo', 'DrMundo_Square_0.png'),
(37, 'Sona', 'Sona_Square_0.png'),
(38, 'Kassadin', 'Kassadin_Square_0.png'),
(39, 'Irelia', 'Irelia_Square_0.png'),
(40, 'Janna', 'Janna_Square_0.png'),
(41, 'Gangplank', 'Gangplank_Square_0.png'),
(42, 'Corki', 'Corki_Square_0.png'),
(43, 'Karma', 'Karma_Square_0.png'),
(44, 'Taric', 'Taric_Square_0.png'),
(45, 'Veigar', 'Veigar_Square_0.png'),
(48, 'Trundle', 'Trundle_Square_0.png'),
(50, 'Swain', 'Swain_Square_0.png'),
(51, 'Caitlyn', 'Caitlyn_Square_0.png'),
(53, 'Blitzcrank', 'Blitzcrank_Square_0.png'),
(54, 'Malphite', 'Malphite_Square_0.png'),
(55, 'Katarina', 'Katarina_Square_0.png'),
(56, 'Nocturne', 'Nocturne_Square_0.png'),
(57, 'Maokai', 'Maokai_Square_0.png'),
(58, 'Renekton', 'Renekton_Square_0.png'),
(59, 'Jarvan IV', 'JarvanIV_Square_0.png'),
(60, 'Elise', 'Elise_Square_0.png'),
(61, 'Orianna', 'Orianna_Square_0.png'),
(62, 'Wukong', 'MonkeyKing_Square_0.png'),
(63, 'Brand', 'Brand_Square_0.png'),
(64, 'LeeSin', 'LeeSin_Square_0.png'),
(67, 'Vayne', 'Vayne_Square_0.png'),
(68, 'Rumble', 'Rumble_Square_0.png'),
(69, 'Cassiopeia', 'Cassiopeia_Square_0.png'),
(72, 'Skarner', 'Skarner_Square_0.png'),
(74, 'Heimerdinger', 'Heimerdinger_Square_0.png'),
(75, 'Nasus', 'Nasus_Square_0.png'),
(76, 'Nidalee', 'Nidalee_Square_0.png'),
(77, 'Udyr', 'Udyr_Square_0.png'),
(78, 'Poppy', 'Poppy_Square_0.png'),
(79, 'Gragas', 'Gragas_Square_0.png'),
(80, 'Pantheon', 'Pantheon_Square_0.png'),
(81, 'Ezreal', 'Ezreal_Square_0.png'),
(82, 'Mordekaiser', 'Mordekaiser_Square_0.png'),
(83, 'Yorick', 'Yorick_Square_0.png'),
(84, 'Akali', 'Akali_Square_0.png'),
(85, 'Kennen', 'Kennen_Square_0.png'),
(86, 'Garen', 'Garen_Square_0.png'),
(89, 'Leona', 'Leona_Square_0.png'),
(90, 'Malzahar', 'Malzahar_Square_0.png'),
(91, 'Talon', 'Talon_Square_0.png'),
(96, 'KogMaw', 'KogMaw_Square_0.png'),
(98, 'Shen', 'Shen_Square_0.png'),
(99, 'Lux', 'Lux_Square_0.png'),
(101, 'Xerath', 'Xerath_Square_0.png'),
(102, 'Shyvana', 'Shyvana_Square_0.png'),
(103, 'Ahri', 'Ahri_Square_0.png'),
(104, 'Graves', 'Graves_Square_0.png'),
(105, 'Fizz', 'Fizz_Square_0.png'),
(106, 'Volibear', 'Volibear_Square_0.png'),
(107, 'Rengar', 'Rengar_Square_0.png'),
(110, 'Varus', 'Varus_Square_0.png'),
(111, 'Nautilus', 'Nautilus_Square_0.png'),
(112, 'Viktor', 'Viktor_Square_0.png'),
(113, 'Sejuani', 'Sejuani_Square_0.png'),
(114, 'Fiora', 'Fiora_Square_0.png'),
(115, 'Ziggs', 'Ziggs_Square_0.png'),
(117, 'Lulu', 'Lulu_Square_0.png'),
(119, 'Draven', 'Draven_Square_0.png'),
(121, 'Khazix', 'Khazix_Square_0.png'),
(122, 'Darius', 'Darius_Square_0.png'),
(126, 'Jayce', 'Jayce_Square_0.png'),
(127, 'Lissandra', 'Lissandra_Square_0.png'),
(131, 'Diana', 'Diana_Square_0.png'),
(133, 'Quinn', 'Quinn_Square_0.png'),
(134, 'Syndra', 'Syndra_Square_0.png'),
(143, 'Zyra', 'Zyra_Square_0.png'),
(150, 'Gnar', 'Gnar_Square_0.png'),
(154, 'Zac', 'Zac_Square_0.png'),
(157, 'Yasuo', 'Yasuo_Square_0.png'),
(161, 'Vel''Koz', 'Velkoz_Square_0.png'),
(201, 'Braum', 'Braum_Square_0.png'),
(222, 'Jinx', 'Jinx_Square_0.png'),
(236, 'Lucian', 'Lucian_Square_0.png'),
(238, 'Zed', 'Zed_Square_0.png'),
(254, 'Vi', 'Vi_Square_0.png'),
(266, 'Aatrox', 'Aatrox_Square_0.png'),
(267, 'Nami', 'Nami_Square_0.png'),
(268, 'Azir', 'Azir_Square_0.png'),
(412, 'Thresh', 'Thresh_Square_0.png'),
(421, 'Rek''Sai', 'RekSai_Square_0.png'),
(429, 'Kalista', 'Kalista_Square_0.png'),
(432, 'Bard', 'Bard_Square_0.png');

-- --------------------------------------------------------

--
-- Table structure for table `champion_has_tag`
--

CREATE TABLE IF NOT EXISTS `champion_has_tag` (
  `Champion_idChampion` int(10) unsigned NOT NULL,
  `Tag_idTag` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Champion_idChampion`,`Tag_idTag`),
  KEY `fk_Champion_has_Tag_Tag1_idx` (`Tag_idTag`),
  KEY `fk_Champion_has_Tag_Champion1_idx` (`Champion_idChampion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `champion_has_tag`
--

INSERT INTO `champion_has_tag` (`Champion_idChampion`, `Tag_idTag`) VALUES
(1, 4),
(1, 5),
(13, 5),
(13, 9),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `idTag` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `idCategoryOfTag` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idTag`,`idCategoryOfTag`),
  KEY `idCategoryOfTag_idx` (`idCategoryOfTag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`idTag`, `name`, `idCategoryOfTag`) VALUES
(1, 'Range', 2),
(2, 'Fighter', 2),
(3, 'Tank', 2),
(4, 'Support', 2),
(5, 'Mage', 2),
(6, 'Assassin', 2),
(7, 'Slow', 1),
(8, 'Own by Aga', 3),
(9, 'Top', 4),
(10, 'Bottom', 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `champion_has_tag`
--
ALTER TABLE `champion_has_tag`
  ADD CONSTRAINT `fk_Champion_has_Tag_Champion1` FOREIGN KEY (`Champion_idChampion`) REFERENCES `champion` (`idChampion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Champion_has_Tag_Tag1` FOREIGN KEY (`Tag_idTag`) REFERENCES `tag` (`idTag`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tag`
--
ALTER TABLE `tag`
  ADD CONSTRAINT `idCategory` FOREIGN KEY (`idCategoryOfTag`) REFERENCES `category` (`idCategory`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
